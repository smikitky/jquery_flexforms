var filterEditor;
(function (filterEditor) {
    filterEditor.options = {
        keys: [{ key: 'name', type: 'text' }, { key: 'age', type: 'number' }]
    };
    var _typeOpMap = {
        text: [
            { op: '=', label: 'is' },
            { op: '!=', label: 'is not' },
            { op: '*=', label: 'contains' },
            { op: '^=', label: 'begins with' },
            { op: '$=', label: 'ends with' }
        ],
        number: [
            { op: '=', label: '=' },
            { op: '>', label: '>' },
            { op: '<', label: '<' },
            { op: '>=', label: '>=' },
            { op: '<=', label: '<=' }
        ],
        select: [
            { op: '=', label: 'is' },
            { op: '!=', label: 'is not' }
        ],
        date: [
            { op: '=', label: 'is' },
            { op: '>', label: '>' },
            { op: '<', label: '<' },
            { op: '>=', label: '>=' },
            { op: '<=', label: '<=' }
        ]
    };
    function _create() {
        var _this = this;
        this.element.empty();
        _createKeySelect.call(this);
        this.groupSelect = $('<select class="ui-filtereditor-group-select"><option>and</option><option>or</option></select>');
        this.toolbar = _createToolbar().appendTo(this.element);
        this.toolbar.on('click', '.ui-filtereditor-toolbutton', $.proxy(_toolButtonClicked, this));
        if (!$.isPlainObject(filterEditor.options.dummyCondition)) {
            this._setOption('dummyCondition', _defaultDummyCondition.call(this));
        }
        if (!$.isPlainObject(filterEditor.options.filter)) {
            filterEditor.options.filter = {
                group: 'and',
                members: [this.options.dummyCondition]
            };
        }
        _reset.call(this);
        this.element.on('mouseenter', '.ui-filtereditor-comparison-node', function (event) { return nodeMouseEnter.call(_this, event); });
        this.element.on('mouseleave', '.ui-filtereditor-node', function (event) { return nodeMouseLeave.call(_this, event); });
        this.element.on('valuechange change', $.proxy(_filterChanged, this));
    }
    filterEditor._create = _create;
    function _defaultDummyCondition() {
        var options = this.options;
        return {
            key: options.keys[0].key,
            condition: '=',
            value: ''
        };
    }
    function _reset() {
        this._setOption('dummyCondition', _defaultDummyCondition.call(this));
        this._setOption('filter', {
            group: 'and',
            members: [this.options.dummyCondition]
        });
    }
    function _createKeySelect() {
        var _this = this;
        var keySelect = $('<select>').addClass('ui-filtereditor-key-select');
        var keys = this.options.keys;
        this.keymap = {};
        $.each(keys, function (i, keydef) {
            if (!(keydef.type in _typeOpMap))
                throw 'Unsupported key type';
            var label = keydef.key.replace('_', ' ');
            if ('label' in keydef)
                label = keydef.label;
            $('<option>').attr('value', keys[i].key).text(label).appendTo(keySelect);
            _this.keymap[keydef.key] = keydef;
        });
        this.keySelect = keySelect;
    }
    function nodeMouseEnter(event) {
        var node = $(event.currentTarget);
        if (node != this.hoveringNode) {
            if (this.hoveringNode)
                this.hoveringNode.removeClass('ui-filtereditor-hover-node');
            if (node.parents('.ui-filtereditor-node').length == 0) {
                this.toolbar.hide();
                this.hoveringNode = null;
            }
            else {
                this.toolbar.show().appendTo(node).position({
                    of: node,
                    at: 'right middle',
                    my: 'right middle',
                    offset: '0 0'
                });
                this.hoveringNode = node;
                this.hoveringNode.addClass('ui-filtereditor-hover-node');
            }
        }
    }
    function nodeMouseLeave(event) {
        if (this.hoveringNode)
            this.hoveringNode.removeClass('ui-filtereditor-hover-node');
        this.toolbar.hide();
        this.hoveringNode = null;
    }
    function _createToolbar() {
        var result = $('<div>').addClass('ui-filtereditor-toolbar');
        var buttons = [
            ['move-up', 'ui-icon-carat-1-n'],
            ['move-down', 'ui-icon-carat-1-s'],
            ['condition-add', 'ui-icon-plusthick'],
            ['condition-addgroup', 'ui-icon-folder-open'],
            ['condition-delete', 'ui-icon-minusthick']
        ];
        $.each(buttons, function (i, item) {
            var button = $('<button>').button({ icons: { primary: item[1] } });
            button.addClass('ui-filtereditor-toolbutton ui-filtereditor-' + item[0]);
            button.appendTo(result);
        });
        return result;
    }
    function _commitFilter() {
        this.root = createElementFromNode(this, this.options.filter);
        this.toolbar.hide().appendTo(this.element);
        this.element.find('.ui-filtereditor-node').remove();
        this.element.append(this.root);
    }
    function _filterChanged() {
        this.toolbar.hide();
        this.options.filter = createNodeFromElement(this.root);
        this.element.trigger('filterchange');
    }
    function _toolButtonClicked(event) {
        var node = this.hoveringNode;
        var button = $(event.currentTarget);
        if (!node || !button.is('button'))
            return;
        if (button.is('.ui-filtereditor-move-up')) {
            var prev = node.prev('.ui-filtereditor-node');
            if (prev) {
                node.insertBefore(prev);
                _filterChanged.call(this);
            }
        }
        if (button.is('.ui-filtereditor-move-down')) {
            var next = node.next('.ui-filtereditor-node');
            if (next) {
                node.insertAfter(next);
                _filterChanged.call(this);
            }
        }
        if (button.is('.ui-filtereditor-condition-add')) {
            var newElement = createElementFromNode(this, this.options.dummyCondition);
            if (node.is('.ui-filtereditor-group-node')) {
                newElement.appendTo(node);
            }
            else {
                newElement.insertAfter(node);
            }
            _filterChanged.call(this);
        }
        if (button.is('.ui-filtereditor-condition-addgroup')) {
            var newElement = createElementFromNode(this, { group: 'and', members: [this.options.dummyCondition] });
            if (node.is('.ui-filtereditor-group-node')) {
                newElement.appendTo(node);
            }
            else {
                newElement.insertAfter(node);
            }
            _filterChanged.call(this);
        }
        if (button.is('.ui-filtereditor-condition-delete')) {
            if (node.is('.ui-filtereditor-comparison-node')) {
                if (node.siblings('.ui-filtereditor-node').length == 0) {
                    var group = node.parent('.ui-filtereditor-group-node');
                    this._toolbuttonClicked(group, button);
                    return;
                }
            }
            if (node.get()[0] == this.root.get()[0]) {
                return;
            }
            this.toolbar.hide().appendTo(this.element);
            node.remove();
            _filterChanged.call(this);
        }
    }
    function _setOption(key, value) {
        $.Widget.prototype._setOption.apply(this, arguments);
        switch (key) {
            case 'filter':
                _commitFilter.call(this);
                break;
            case 'keys':
                _createKeySelect.call(this);
                _reset.call(this);
                break;
        }
    }
    filterEditor._setOption = _setOption;
    function createElementFromNode(widget, node) {
        var self = widget;
        function createElementFromGroupNode(node) {
            var elem = $('<div>').addClass('ui-filtereditor-group-node ui-filtereditor-node');
            self.groupSelect.clone().val(node.group).appendTo(elem);
            $.each(node.members, function (i, member) {
                var child = createElementFromNode(self, member);
                child.appendTo(elem);
            });
            elem.sortable({
                axis: 'y',
                start: function () {
                    self.toolbar.hide();
                },
                update: $.proxy(_filterChanged, self),
                items: '> .ui-filtereditor-node'
            });
            return elem;
        }
        function createTypedFieldAndOperatorSelector(selectedKey, currentValue) {
            var op = $('<select>').addClass('ui-filtereditor-operation-select');
            var keydef = this.keymap[selectedKey];
            $.each(_typeOpMap[keydef.type], function (i, opdef) {
                $('<option>').text(opdef.label).attr('value', opdef.op).appendTo(op);
            });
            var opt = { type: keydef.type, spec: keydef.spec };
            var value = $('<span>').addClass('ui-filtereditor-value').typedfield(opt);
            if (typeof currentValue !== 'undefined') {
                value.typedfield('option', 'value', currentValue);
            }
            return { op: op, value: value };
        }
        function createElementFromComparisonNode(node) {
            var elem = $('<div>').addClass('ui-filtereditor-comparison-node ui-filtereditor-node');
            var tmpKey = self.keySelect.clone().val([node.key]);
            var op_val = createTypedFieldAndOperatorSelector.call(self, node.key, node.value);
            op_val.value.val(node.condition);
            tmpKey.on('change', function (event) {
                $(event.target).siblings('.ui-filtereditor-operation-select, .ui-filtereditor-value').remove();
                var key = $(event.target).val();
                var op_val = createTypedFieldAndOperatorSelector.call(self, key, undefined);
                elem.append(op_val.op, op_val.value);
            });
            elem.append(tmpKey, op_val.op, op_val.value);
            return elem;
        }
        if (node.members instanceof Array)
            return createElementFromGroupNode(node);
        else if (node.key !== undefined)
            return createElementFromComparisonNode(node);
        else
            throw 'Error';
    }
    function createNodeFromElement(element) {
        function createNodeFromGroupElement(element) {
            var members = [];
            element.children('.ui-filtereditor-node').each(function () {
                var item = createNodeFromElement($(this));
                if (item != null)
                    members.push(item);
            });
            var groupType = $('.ui-filtereditor-group-select', element).val();
            if (members.length > 0)
                return { group: groupType, members: members };
            else
                return null;
        }
        function createNodeFromComparisonElement(element) {
            return {
                key: element.find('.ui-filtereditor-key-select').val(),
                condition: element.find('.ui-filtereditor-operation-select').val(),
                value: element.find('.ui-filtereditor-value').typedfield('option', 'value')
            };
        }
        if (element.is('.ui-filtereditor-group-node'))
            return createNodeFromGroupElement(element);
        else if (element.is('.ui-filtereditor-comparison-node'))
            return createNodeFromComparisonElement(element);
        else
            throw "exception";
    }
    function exportMongo(element) {
        var self = this;
        function createNodeFromGroupElement(element) {
            var members = [];
            element.children('.ui-filtereditor-node').each(function () {
                var item = self.exportMongo($(this));
                if (item != null)
                    members.push(item);
            });
            var groupType = $('.ui-filtereditor-group-select', element).val();
            if (members.length > 0) {
                if (groupType == 'and')
                    return { $and: members };
                if (groupType == 'or')
                    return { $or: members };
            }
            else {
                return null;
            }
        }
        function escapeRegex(str) {
            return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
        }
        function createNodeFromComparisonElement(element) {
            var result = {};
            var key = element.find('.ui-filtereditor-key-select').val();
            var cond = element.find('.ui-filtereditor-operation-select').val();
            var value = element.find('.ui-filtereditor-value').typedfield('option', 'value');
            if (self.keymap[key].type === 'date') {
                var offset = -(new Date()).getTimezoneOffset();
                var sign = offset >= 0 ? '+' : '-';
                offset = Math.abs(offset);
                var hour = ('00' + Math.floor(offset / 60).toString()).slice(-2);
                var min = ('00' + (offset % 60).toString()).slice(-2);
                value = { '$date': value + 'T00:00:00' + sign + hour + ':' + min };
            }
            switch (cond) {
                case '=':
                    result[key] = value;
                    break;
                case '>':
                    result[key] = { $gt: value };
                    break;
                case '<':
                    result[key] = { $lt: value };
                    break;
                case '>=':
                    result[key] = { $gte: value };
                    break;
                case '<=':
                    result[key] = { $lte: value };
                    break;
                case '!=':
                    result[key] = { $ne: value };
                    break;
                case '*=':
                    result[key] = { $regex: escapeRegex(value) };
                    break;
                case '^=':
                    result[key] = { $regex: '^' + escapeRegex(value) };
                    break;
                case '$=':
                    result[key] = { $regex: escapeRegex(value) + '$' };
                    break;
            }
            return result;
        }
        if (!element)
            element = self.root;
        if (element.is('.ui-filtereditor-group-node'))
            return createNodeFromGroupElement(element);
        else if (element.is('.ui-filtereditor-comparison-node'))
            return createNodeFromComparisonElement(element);
        else
            throw "exception";
    }
    filterEditor.exportMongo = exportMongo;
})(filterEditor || (filterEditor = {}));
$.widget('ui.filtereditor', filterEditor);
