var typedFieldWidget;
(function (typedFieldWidget) {
    typedFieldWidget.options = {
        type: 'text',
        spec: {}
    };
    function _create() {
        createInput.call(this);
    }
    typedFieldWidget._create = _create;
    var map = {
        text: typedField.TextInput,
        password: typedField.PasswordInput,
        number: typedField.NumberInput,
        json: typedField.Json,
        select: typedField.Select,
        selectmultiple: typedField.SelectMultiple,
        checkboxgroup: typedField.CheckBoxGroup,
        date: typedField.DatePicker,
        checkbox: typedField.CheckBox,
        radio: typedField.RadioGroup,
        list: typedField.ArrayList,
        form: typedField.Form,
        callback: typedField.Callback
    };
    function registerType(type, classDefinition) {
        map[type] = classDefinition;
    }
    typedFieldWidget.registerType = registerType;
    function createInput() {
        var _this = this;
        this.element.empty();
        this.element.addClass('ui-typedfield ui-typedfield-' + this.options.type);
        if (!(this.options.type in map)) {
            throw 'Undefined field type';
        }
        this.field = new map[this.options.type](this.options.spec);
        var elem = this.field.createElement();
        if ('value' in this.options) {
            this.field.set(this.options.value);
        }
        else if ('default' in this.options.spec) {
            this.field.set(this.options.spec.default);
        }
        _validate.call(this);
        this.field.changed = function () {
            _validate.call(_this);
            _this.element.trigger('valuechange', [_this.field]);
        };
        elem.appendTo(this.element);
    }
    function _validate() {
        var valid = this.field.valid();
        this.element.toggleClass('ui-typedfield-invalid', !valid);
        this.options.value = valid ? this.field.get() : null;
    }
    function enable() {
        this.field.enable();
        this.options.disabled = false;
    }
    typedFieldWidget.enable = enable;
    function disable() {
        this.field.disable();
        this.options.disabled = true;
    }
    typedFieldWidget.disable = disable;
    function getValue() {
        return this.options.value;
    }
    typedFieldWidget.getValue = getValue;
    function valid() {
        return this.field.valid();
    }
    typedFieldWidget.valid = valid;
    function setValue(value) {
        this.field.set(value);
        _validate.call(this);
    }
    typedFieldWidget.setValue = setValue;
    function reset() {
        this.field.reset();
        this.field.changed();
    }
    typedFieldWidget.reset = reset;
    function _setOption(key, value) {
        switch (key) {
            case 'type':
                this.options.type = value;
                createInput.call(this);
                break;
            case 'spec':
                this.options.spec = value;
                createInput.call(this);
                break;
            case 'value':
                setValue.call(this, value);
                break;
            case 'disabled':
                if (!!(value)) {
                    this.enable();
                }
                else {
                    this.disable();
                }
                break;
            default:
                this._super(key, value);
        }
    }
    typedFieldWidget._setOption = _setOption;
})(typedFieldWidget || (typedFieldWidget = {}));
$.widget("ui.typedfield", typedFieldWidget);
