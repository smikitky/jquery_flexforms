var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var typedField;
(function (typedField) {
    var Base = (function () {
        function Base(spec) {
            this.spec = $.extend({}, spec);
        }
        Base.prototype.splitKeyLabel = function (input) {
            var m = null;
            if (m = input.match(/^(.+?)\:(.+)$/)) {
                return [m[1], m[2]];
            }
            else {
                return [input, input];
            }
        };
        Base.prototype.assignSpecsToElementProp = function (keys) {
            var _this = this;
            $.each(keys, function (i, key) {
                if (key in _this.spec)
                    _this.element.prop(key, _this.spec[key]);
            });
        };
        Base.prototype.triggerChanged = function () {
            if (typeof this.changed === 'function') {
                this.changed();
            }
        };
        Base.prototype.convertScalar = function (data) {
            if (!('spec' in this) || !('valueType' in this.spec)) {
                return data;
            }
            switch (this.spec.valueType) {
                case 'int':
                    return parseInt(data);
                case 'number':
                    return parseFloat(data);
                case 'boolean':
                    return !!data;
                case 'string':
                    return data.toString();
                default:
                    return data;
            }
        };
        Base.prototype.createElement = function () {
            return null;
        };
        Base.prototype.enable = function () {
        };
        Base.prototype.disable = function () {
        };
        Base.prototype.disabled = function () {
            return false;
        };
        Base.prototype.valid = function () {
            return true;
        };
        Base.prototype.get = function () {
        };
        Base.prototype.set = function (value) {
        };
        Base.prototype.reset = function () {
            this.set('');
        };
        return Base;
    })();
    typedField.Base = Base;
    var HtmlInputInput = (function (_super) {
        __extends(HtmlInputInput, _super);
        function HtmlInputInput() {
            _super.apply(this, arguments);
        }
        HtmlInputInput.prototype.get = function () {
            return this.element.val();
        };
        HtmlInputInput.prototype.set = function (value) {
            this.element.val(value);
        };
        HtmlInputInput.prototype.enable = function () {
            this.element.prop('disabled', false);
        };
        HtmlInputInput.prototype.disable = function () {
            this.element.prop('disabled', true);
        };
        HtmlInputInput.prototype.disabled = function () {
            return this.element.prop('disabled');
        };
        return HtmlInputInput;
    })(Base);
    typedField.HtmlInputInput = HtmlInputInput;
    var TextInput = (function (_super) {
        __extends(TextInput, _super);
        function TextInput() {
            _super.apply(this, arguments);
        }
        TextInput.prototype.createElement = function () {
            var _this = this;
            this.element = $('<input type="text">').addClass('ui-typedfield-text');
            this.assignSpecsToElementProp(['placeholder', 'length']);
            this.element.on('change input keyup', function (event) {
                _this.triggerChanged();
                event.stopPropagation();
            });
            return this.element;
        };
        TextInput.prototype.valid = function () {
            if ('regex' in this.spec) {
                var match = this.element.val().match(this.spec.regex);
                return !!match;
            }
            return true;
        };
        return TextInput;
    })(HtmlInputInput);
    typedField.TextInput = TextInput;
    var PasswordInput = (function (_super) {
        __extends(PasswordInput, _super);
        function PasswordInput() {
            _super.apply(this, arguments);
        }
        PasswordInput.prototype.createElement = function () {
            _super.prototype.createElement.call(this);
            this.element.attr('type', 'password');
            return this.element;
        };
        return PasswordInput;
    })(TextInput);
    typedField.PasswordInput = PasswordInput;
    var NumberInput = (function (_super) {
        __extends(NumberInput, _super);
        function NumberInput() {
            _super.apply(this, arguments);
            this.html5 = null;
        }
        NumberInput.prototype.htmlNumberSupported = function () {
            if (NumberInput.numberSupported === null) {
                var tmp = $('<input type="number">');
                NumberInput.numberSupported = tmp.prop('type') === 'number';
            }
            return NumberInput.numberSupported;
        };
        NumberInput.prototype.valid = function () {
            var val = this.element.val();
            if (!val.match(/^-?(0|[1-9]\d*)(\.\d+)?$/))
                return false;
            if ('min' in this.spec && this.spec.min > val)
                return false;
            if ('max' in this.spec && this.spec.max < val)
                return false;
            if (!this.spec.float && val.match(/\./))
                return false;
            return true;
        };
        NumberInput.prototype.get = function () {
            if (this.spec.float === true) {
                return parseFloat(this.element.val());
            }
            else {
                return parseInt(this.element.val());
            }
        };
        NumberInput.prototype.set = function (value) {
            this.element.val(parseInt(value).toString());
        };
        NumberInput.prototype.createElement = function () {
            var _this = this;
            this.element = $('<input>');
            var useHtml5 = this.htmlNumberSupported && this.spec.html5 !== false;
            this.element.prop('type', useHtml5 ? 'number' : 'text');
            this.assignSpecsToElementProp(['placeholder', 'min', 'max', 'step']);
            this.element.on('change input keyup', function () {
                _this.triggerChanged();
                event.stopPropagation();
            });
            return this.element;
        };
        NumberInput.numberSupported = null;
        return NumberInput;
    })(HtmlInputInput);
    typedField.NumberInput = NumberInput;
    var Json = (function (_super) {
        __extends(Json, _super);
        function Json() {
            _super.apply(this, arguments);
        }
        Json.prototype.createElement = function () {
            var _this = this;
            this.element = $('<textarea>').addClass('ui-typedfield-textarea ui-typedfield-json');
            this.element.on('change input keyup', function (event) {
                _this.triggerChanged();
                event.stopPropagation();
            });
            return this.element;
        };
        Json.prototype.enable = function () {
            this.element.prop('disabled', false);
        };
        Json.prototype.disable = function () {
            this.element.prop('disabled', true);
        };
        Json.prototype.disabled = function () {
            return this.element.prop('disabled');
        };
        Json.prototype.valid = function () {
            var val = this.element.val();
            var tmp = null;
            try {
                tmp = JSON.parse(val);
            }
            catch (e) {
                return false;
            }
            return true;
        };
        Json.prototype.get = function () {
            var val = this.element.val();
            return JSON.parse(val);
        };
        Json.prototype.set = function (value) {
            this.element.val(JSON.stringify(value, null, '\t'));
        };
        Json.prototype.reset = function () {
            this.set('');
        };
        return Json;
    })(Base);
    typedField.Json = Json;
    var Select = (function (_super) {
        __extends(Select, _super);
        function Select() {
            _super.apply(this, arguments);
        }
        Select.prototype.createElement = function () {
            var _this = this;
            this.element = $('<select>').addClass('ui-tf-select');
            this.element.on('change', function () {
                _this.triggerChanged();
            });
            if ($.isArray(this.spec.options)) {
                var options = this.spec.options;
                for (var i = 0; i < options.length; i++) {
                    var tmp = this.splitKeyLabel(options[i]);
                    $('<option>').prop('value', tmp[0]).text(tmp[1]).appendTo(this.element);
                }
            }
            return this.element;
        };
        Select.prototype.get = function () {
            return this.convertScalar(this.element.val());
        };
        return Select;
    })(HtmlInputInput);
    typedField.Select = Select;
    var SelectMultiple = (function (_super) {
        __extends(SelectMultiple, _super);
        function SelectMultiple() {
            _super.apply(this, arguments);
        }
        SelectMultiple.prototype.createElement = function () {
            var element = _super.prototype.createElement.call(this);
            element.prop('multiple', true);
            return element;
        };
        SelectMultiple.prototype.get = function () {
            var values = this.element.val();
            if (values === null)
                return [];
            var result = [];
            for (var i = 0; i < values.length; i++) {
                result.push(this.convertScalar(values[i]));
            }
            return result;
        };
        return SelectMultiple;
    })(Select);
    typedField.SelectMultiple = SelectMultiple;
    var InputArrayBase = (function (_super) {
        __extends(InputArrayBase, _super);
        function InputArrayBase() {
            _super.apply(this, arguments);
            this.inputType = 'radio';
        }
        InputArrayBase.prototype.createElement = function () {
            this.element = $('<div>');
            if (!$.isArray(this.spec.options)) {
                return this.element;
            }
            var options = this.spec.options;
            for (var i = 0; i < options.length; i++) {
                var tmp = this.splitKeyLabel(options[i]);
                var input = $('<input>').prop('type', this.inputType).prop('value', tmp[0]);
                var label = $('<label>').append(input).append(tmp[1]);
                if (this.spec.vertical)
                    label = $('<div>').append(label);
                label.appendTo(this.element);
            }
            return this.element;
        };
        InputArrayBase.prototype.disable = function () {
            this.element.find(':' + this.inputType).prop('disabled', true);
        };
        InputArrayBase.prototype.enable = function () {
            this.element.find(':' + this.inputType).prop('disabled', false);
        };
        return InputArrayBase;
    })(Base);
    typedField.InputArrayBase = InputArrayBase;
    var RadioGroup = (function (_super) {
        __extends(RadioGroup, _super);
        function RadioGroup() {
            _super.apply(this, arguments);
        }
        RadioGroup.prototype.createElement = function () {
            var _this = this;
            _super.prototype.createElement.call(this);
            this.element.on('click', '> label > :radio', function (event) {
                _this.element.find('> label > :radio').each(function (i, radio) {
                    if (event.target !== radio)
                        $(radio).prop('checked', false);
                });
                _this.triggerChanged();
            });
            return this.element;
        };
        RadioGroup.prototype.valid = function () {
            return this.element.find(':radio:checked').length > 0;
        };
        RadioGroup.prototype.get = function () {
            return this.convertScalar(this.element.find('> label > :radio:checked').val());
        };
        RadioGroup.prototype.set = function (value) {
            this.element.find('> label > :radio').each(function (i, radio) {
                $(radio).prop('checked', $(radio).val() == value);
            });
        };
        return RadioGroup;
    })(InputArrayBase);
    typedField.RadioGroup = RadioGroup;
    var CheckBoxGroup = (function (_super) {
        __extends(CheckBoxGroup, _super);
        function CheckBoxGroup() {
            _super.apply(this, arguments);
            this.inputType = 'checkbox';
        }
        CheckBoxGroup.prototype.createElement = function () {
            var _this = this;
            _super.prototype.createElement.call(this);
            this.element.on('click', ':checkbox', function () {
                _this.triggerChanged();
            });
            return this.element;
        };
        CheckBoxGroup.prototype.get = function () {
            var _this = this;
            var result = [];
            this.element.find(':checkbox').each(function (i, item) {
                var cb = $(item);
                if ($(cb).is(':checked'))
                    result.push(_this.convertScalar(cb.prop('value')));
            });
            return result;
        };
        CheckBoxGroup.prototype.set = function (value) {
            var _this = this;
            this.element.find(':checkbox').prop('checked', false);
            if (!$.isArray(value))
                return;
            $.each(value, function (i, item) {
                _this.element.find(':checkbox').filter(function (j, cb) { return $(cb).prop('value') == item; }).prop('checked', true);
            });
        };
        return CheckBoxGroup;
    })(InputArrayBase);
    typedField.CheckBoxGroup = CheckBoxGroup;
    var DatePicker = (function (_super) {
        __extends(DatePicker, _super);
        function DatePicker() {
            _super.apply(this, arguments);
        }
        DatePicker.prototype.createElement = function () {
            var _this = this;
            this.element = $('<input>').datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: function () { return _this.triggerChanged(); }
            });
            return this.element;
        };
        DatePicker.prototype.enable = function () {
            this.element.datepicker('enable');
        };
        DatePicker.prototype.disable = function () {
            this.element.datepicker('disable');
        };
        DatePicker.prototype.disabled = function () {
            return this.element.datepicker('disabled');
        };
        DatePicker.prototype.get = function () {
            return this.element.val();
        };
        DatePicker.prototype.set = function (value) {
            this.element.datepicker('setDate', value);
        };
        return DatePicker;
    })(Base);
    typedField.DatePicker = DatePicker;
    var CheckBox = (function (_super) {
        __extends(CheckBox, _super);
        function CheckBox() {
            _super.apply(this, arguments);
        }
        CheckBox.prototype.createElement = function () {
            var _this = this;
            this.element = $('<label>');
            this.checkbox = $('<input>').attr('type', 'checkbox').appendTo(this.element);
            if ('label' in this.spec && typeof this.spec.label == 'string') {
                this.element.append(this.spec.label);
            }
            this.checkbox.on('change', function () {
                _this.triggerChanged();
            });
            return this.element;
        };
        CheckBox.prototype.enable = function () {
            this.checkbox.prop('disabled', false);
        };
        CheckBox.prototype.disable = function () {
            this.checkbox.prop('disabled', true);
        };
        CheckBox.prototype.disabled = function () {
            return this.checkbox.prop('disabled');
        };
        CheckBox.prototype.get = function () {
            return this.checkbox.prop('checked');
        };
        CheckBox.prototype.set = function (value) {
            this.checkbox.prop('checked', !!value);
        };
        return CheckBox;
    })(Base);
    typedField.CheckBox = CheckBox;
    var ArrayList = (function (_super) {
        __extends(ArrayList, _super);
        function ArrayList() {
            _super.apply(this, arguments);
            this.isDisabled = false;
        }
        ArrayList.prototype.createElement = function () {
            var _this = this;
            var elem = $('<div>');
            this.add = $('<a>').addClass('ui-icon ui-icon-plusthick').appendTo(elem).on('click', function () {
                _this.addElement();
                _this.triggerChanged();
            });
            this.fields = $('<div>').addClass('ui-typedfield-field-list').appendTo(elem);
            this.fields.sortable({
                axis: 'y',
                update: this.triggerChanged.bind(this),
                handle: '.ui-typedfield-list-grip',
                containment: this.fields
            });
            this.element = elem;
            return elem;
        };
        ArrayList.prototype.addElement = function (value) {
            var _this = this;
            if (value === void 0) { value = undefined; }
            var container = $('<div>').addClass('ui-typedfield-list-container').appendTo(this.fields);
            $('<span>').addClass('ui-icon ui-typedfield-list-grip ui-icon-grip-dotted-vertical').appendTo(container);
            $('<span>').addClass('ui-icon ui-typedfield-list-delete ui-icon-close').appendTo(container).on('click', function () {
                container.remove();
                _this.triggerChanged();
            });
            var field = $('<span>').typedfield({
                type: this.spec.elementType,
                spec: this.spec.elementSpec
            }).appendTo(container);
            if (typeof value !== 'undefined') {
                field.typedfield('setValue', value);
            }
            field.on('valuechange', function (event) {
                event.stopPropagation();
                _this.triggerChanged();
            });
        };
        ArrayList.prototype.allFields = function () {
            return this.fields.find('.ui-typedfield-list-container > .ui-typedfield');
        };
        ArrayList.prototype.reset = function () {
            this.fields.empty();
        };
        ArrayList.prototype.get = function () {
            var _this = this;
            if (this.spec.key) {
                var result = {};
                this.allFields().get().forEach(function (f) {
                    var data = $.extend({}, $(f).typedfield('getValue'));
                    var key = data[_this.spec.key];
                    delete data[_this.spec.key];
                    result[key] = data;
                });
                return result;
            }
            else {
                return this.allFields().get().map(function (f) { return $(f).typedfield('getValue'); });
            }
        };
        ArrayList.prototype.set = function (value) {
            var _this = this;
            this.reset();
            if (this.spec.key) {
                if ($.isPlainObject(value)) {
                    $.each(value, function (key, val) {
                        val[_this.spec.key] = key;
                        _this.addElement(val);
                    });
                }
            }
            else {
                if ($.isArray(value)) {
                    value.forEach(function (val) {
                        _this.addElement(val);
                    });
                }
            }
        };
        ArrayList.prototype.disable = function () {
            this.allFields().each(function (i, f) {
                $(f).typedfield('disable');
            });
            this.isDisabled = true;
        };
        ArrayList.prototype.enable = function () {
            this.allFields().each(function (i, f) {
                $(f).typedfield('enable');
            });
            this.isDisabled = false;
        };
        ArrayList.prototype.disabled = function () {
            return this.isDisabled;
        };
        ArrayList.prototype.valid = function () {
            return !this.allFields().get().some(function (f) {
                return !$(f).typedfield('valid');
            });
        };
        return ArrayList;
    })(Base);
    typedField.ArrayList = ArrayList;
    var Form = (function (_super) {
        __extends(Form, _super);
        function Form() {
            _super.apply(this, arguments);
            this.isDisabled = false;
        }
        Form.prototype.createElement = function () {
            var _this = this;
            this.element = this.spec.form.clone();
            this.element.on('change click keydown input', function (event) {
                event.stopPropagation();
                _this.triggerChanged();
            });
            return this.element;
        };
        Form.prototype.allFields = function () {
            return this.element.find('[name]');
        };
        Form.prototype.get = function () {
            var data = {};
            $.each(this.allFields(), function () {
                if (this.disabled)
                    return;
                if (/select|textarea/i.test(this.nodeName) || /text|hidden|password/i.test(this.type)) {
                    data[this.name] = $(this).val();
                }
                else if (this.checked) {
                    if (data[this.name] == undefined)
                        data[this.name] = [];
                    data[this.name].push($(this).val());
                }
            });
            if (typeof this.spec.filter === 'function')
                data = this.spec.filter(data);
            return data;
        };
        Form.prototype.set = function (value) {
            $.each(this.allFields(), function () {
                if (value[this.name]) {
                    var val = $(this).val();
                    if (/checkbox|radio/i.test(this.type)) {
                        $(this).prop('checked', $.isArray(value[this.name]) && value[this.name].some(function (v) { return v == val; }));
                    }
                    else {
                        $(this).val(value[this.name]);
                    }
                }
                else {
                    $(this).val(null);
                }
            });
        };
        Form.prototype.reset = function () {
            this.set({});
        };
        Form.prototype.enable = function () {
            $.each(this.allFields(), function () {
                $(this).prop('disabled', false);
            });
            this.isDisabled = false;
        };
        Form.prototype.disable = function () {
            $.each(this.allFields(), function () {
                $(this).prop('disabled', true);
            });
            this.isDisabled = true;
        };
        Form.prototype.disabled = function () {
            return this.isDisabled;
        };
        Form.prototype.valid = function () {
            if (typeof this.spec.validator === 'function') {
                return this.spec.validator();
            }
            else {
                return true;
            }
        };
        return Form;
    })(Base);
    typedField.Form = Form;
    var Callback = (function (_super) {
        __extends(Callback, _super);
        function Callback() {
            _super.apply(this, arguments);
            this.data = null;
        }
        Callback.prototype.createElement = function () {
            var _this = this;
            var element = $('<div>');
            this.content = $('<div>').addClass('ui-typedfield-data');
            element.append(this.content);
            this.button = $('<a>').addClass('ui-typedfield-data-button ui-icon ui-icon-pencil');
            this.button.on('click', function () {
                if (typeof _this.spec.edit !== 'function')
                    return;
                _this.spec.edit(_this.data, function (data) {
                    _this.set(data);
                    _this.changed();
                });
            });
            element.append(this.button);
            return element;
        };
        Callback.prototype.get = function () {
            return this.data;
        };
        Callback.prototype.set = function (value) {
            this.data = value;
            if (typeof this.spec.render === 'function')
                this.spec.render(this.content, value);
        };
        Callback.prototype.reset = function () {
            this.set(null);
        };
        Callback.prototype.enable = function () {
            this.button.prop('disabled', false);
        };
        Callback.prototype.disable = function () {
            this.button.prop('disabled', true);
        };
        Callback.prototype.disabled = function () {
            return this.button.prop('disabled');
        };
        return Callback;
    })(Base);
    typedField.Callback = Callback;
})(typedField || (typedField = {}));
