var propertyEditorWidget;
(function (propertyEditorWidget) {
    propertyEditorWidget.options = {
        properties: [],
        value: {},
        useCaptions: true
    };
    function _create() {
        _refreshForm.call(this);
    }
    propertyEditorWidget._create = _create;
    function _refreshForm() {
        var _this = this;
        var props = this.options.properties;
        this.element.empty().addClass('ui-propertyeditor');
        var table = $('<table>').addClass('ui-propertyeditor-table').appendTo(this.element);
        this.fields = {};
        $.each(props, function (i, prop) {
            if (prop.key in _this.fields)
                return;
            if ('heading' in prop) {
                var row = $('<tr>').addClass('ui-propertyeditor-heading');
                var column = $('<th>').attr('colspan', 2).text(prop.heading).appendTo(row);
                row.appendTo(table);
                return;
            }
            var row = $('<tr>').addClass('ui-propertyeditor-row');
            var caption = $('<th>').addClass('ui-propertyeditor-caption').text(prop.caption);
            var editor = $('<td>').addClass('ui-propertyeditor-column').typedfield({ type: prop.type, spec: prop.spec }).on('valuechange', function (event) {
                _this.options.value[prop.key] = $(event.target).typedfield('getValue');
            });
            row.append(caption).append(editor).appendTo(table);
            _this.fields[prop.key] = editor;
        });
        _assignValues.call(this, this.options.value);
        if (this.options.disabled) {
            disable.call(this);
        }
    }
    function enable() {
        this.element.find('.ui-propertyeditor-column').typedfield('enable');
        this.options.disabled = false;
    }
    propertyEditorWidget.enable = enable;
    function disable() {
        this.element.find('.ui-propertyeditor-column').typedfield('disable');
        this.options.disabled = true;
    }
    propertyEditorWidget.disable = disable;
    function valid() {
        var _this = this;
        var props = this.options.properties;
        var result = true;
        $.each(props, function (i, prop) {
            if ('heading' in prop)
                return;
            result = result && _this.fields[prop.key].typedfield('valid');
        });
        return result;
    }
    propertyEditorWidget.valid = valid;
    function complain(messages) {
        var _this = this;
        var key;
        this.undoComplain();
        for (key in messages) {
            messages[key].forEach(function (mes) {
                if (!(key in _this.fields))
                    return;
                var message = $('<p>').addClass('ui-propertyeditor-error').text(mes);
                _this.fields[key].closest('td.ui-propertyeditor-column').append(message);
                _this.fields[key].closest('ui-propertyeditor-row').addClass('ui-propertyeditor-complained');
            });
        }
    }
    propertyEditorWidget.complain = complain;
    function undoComplain() {
        $('.ui-propertyeditor-row', this.element).removeClass('ui-propertyeditor.complained');
        $('.ui-propertyeditor-error', this.element).remove();
    }
    propertyEditorWidget.undoComplain = undoComplain;
    function clear() {
        for (var key in this.fields) {
            this.fields[key].typedfield('reset');
        }
        _refreshValues.call(this);
    }
    propertyEditorWidget.clear = clear;
    function _setOption(key, value) {
        switch (key) {
            case 'value':
                _assignValues.call(this, value);
                break;
            case 'disabled':
                if (!!value) {
                    this.disable();
                }
                else {
                    this.enable();
                }
                break;
            default:
                this._super(key, value);
        }
    }
    propertyEditorWidget._setOption = _setOption;
    function _refreshValues() {
        var _this = this;
        this.options.value = {};
        var props = this.options.properties;
        $.each(props, function (i, prop) {
            if ('heading' in prop)
                return;
            var field = _this.fields[prop.key];
            _this.options.value[prop.key] = field.typedfield('valid') ? field.typedfield('getValue') : null;
        });
    }
    function _assignValues(value) {
        this.undoComplain();
        for (var key in value) {
            if (typeof this.fields[key] === 'object') {
                this.fields[key].typedfield('setValue', value[key]);
            }
        }
        _refreshValues.call(this);
    }
})(propertyEditorWidget || (propertyEditorWidget = {}));
$.widget("ui.propertyeditor", propertyEditorWidget);
