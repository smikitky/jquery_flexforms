// Load gulp tasks

var gulp = require('gulp');
var less = require('gulp-less');
var jade = require('gulp-jade');
var typescript = require('gulp-typescript');
var uglifyjs = require('gulp-uglifyjs');
var header = require('gulp-header');
var concat = require('gulp-concat');

// default task
gulp.task('default', ['less', 'jade', 'typescript']);

gulp.task('less', function () {
  gulp.src('src/*.less')
    .pipe(less())
    .pipe(gulp.dest('dist'));
});

gulp.task('jade', function () {
  gulp.src('demo/index.jade')
    .pipe(jade())
    .pipe(gulp.dest('demo'));
});

gulp.task('typescript', function () {
  var head = '/* jQuery flex forms (C)Soichiro Miki <smiki-tky@umin.ac.jp> */\n';

  gulp.src(['src/*.ts'])
    .pipe(typescript({sortOutput: true, removeComments: true}))
    .pipe(gulp.dest('dist/individual'))
    .pipe(concat('jquery.flexforms.js'))
    .pipe(header(head))
    .pipe(gulp.dest('dist'))
    .pipe(uglifyjs('jquery.flexforms.min.js'))
    .pipe(header(head))
    .pipe(gulp.dest('dist'));
});

// watch
gulp.task('watch', ['default'], function () {
  gulp.watch('src/*.ts', ['typescript']);
  gulp.watch('src/*.less', ['less']);
  gulp.watch('demo/*.jade', ['jade']);
});