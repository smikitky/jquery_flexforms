/// <reference path="typings/jquery/jquery.d.ts" />
/// <reference path="typings/jqueryui/jqueryui.d.ts" />
/// <reference path="jquery.typedfield.ts" />


module filterEditor {

    export var options: any = {
        keys: [{key: 'name', type: 'text'}, {key: 'age', type: 'number'}]
    };

    interface OperatorDefinition {
        op: string;
        label: string;
    }

    interface Node {
        group?: string;
        members?: Node[];
        key?: string;
        condition?: string;
        value?: any;
    }

    interface KeyDefinition {
        key: string;
        type: string;
        label?: string;
        spec: any;
    }

    interface Options {
        keys: KeyDefinition[];
        dummyCondition: Node;
        filter: Node;
    }

    interface KeyMap {
        [key: string]: KeyDefinition;
    }

    var _typeOpMap: {[type: string]: OperatorDefinition[]} = {
        text: [
            {op: '=', label: 'is'},
            {op: '!=', label: 'is not'},
            {op: '*=', label: 'contains'},
            {op: '^=', label: 'begins with'},
            {op: '$=', label: 'ends with'}
        ],
        number: [
            {op: '=', label: '='},
            {op: '>', label: '>'},
            {op: '<', label: '<'},
            {op: '>=', label: '>='},
            {op: '<=', label: '<='}
        ],
        select: [
            {op: '=', label: 'is'},
            {op: '!=', label: 'is not'}
        ],
        date: [
            {op: '=', label: 'is'},
            {op: '>', label: '>'},
            {op: '<', label: '<'},
            {op: '>=', label: '>='},
            {op: '<=', label: '<='}
        ]
    };

    export function _create(): void {
        this.element.empty();

        _createKeySelect.call(this);

        this.groupSelect = $('<select class="ui-filtereditor-group-select"><option>and</option><option>or</option></select>');

        this.toolbar = _createToolbar().appendTo(this.element);
        this.toolbar.on('click', '.ui-filtereditor-toolbutton', $.proxy(_toolButtonClicked, this));

        if (!$.isPlainObject(options.dummyCondition)) {
            this._setOption('dummyCondition', _defaultDummyCondition.call(this));
        }
        if (!$.isPlainObject(options.filter)) {
            options.filter = {
                group: 'and',
                members: [this.options.dummyCondition]
            };
        }
        _reset.call(this);

        this.element.on('mouseenter', '.ui-filtereditor-comparison-node',
            (event) => nodeMouseEnter.call(this, event)
        );
        this.element.on('mouseleave', '.ui-filtereditor-node',
            (event) => nodeMouseLeave.call(this, event)
        );
        this.element.on('valuechange change', $.proxy(_filterChanged, this));
    }

    function _defaultDummyCondition(): any {
        var options = <Options>this.options;
        return {
            key: options.keys[0].key,
            condition: '=',
            value: ''
        };
    }

    function _reset(): void {
        this._setOption('dummyCondition', _defaultDummyCondition.call(this));
        this._setOption('filter', {
            group: 'and',
            members: [this.options.dummyCondition]
        });
    }

    function _createKeySelect(): void {
        var keySelect = $('<select>').addClass('ui-filtereditor-key-select');
        var keys = this.options.keys;
        this.keymap = {};
        $.each(keys, (i, keydef) => {
            if (!(keydef.type in _typeOpMap)) throw 'Unsupported key type';
            var label = keydef.key.replace('_', ' ');
            if ('label' in keydef) label = keydef.label;
            $('<option>').attr('value', keys[i].key).text(label).appendTo(keySelect);
            this.keymap[keydef.key] = keydef;
        });
        this.keySelect = keySelect;
    }

    function nodeMouseEnter(event: Event): void {
        var node = $(event.currentTarget);
        if (node != this.hoveringNode) {
            if (this.hoveringNode) this.hoveringNode.removeClass('ui-filtereditor-hover-node');
            if (node.parents('.ui-filtereditor-node').length == 0) {
                // top level group cannot be changed
                this.toolbar.hide();
                this.hoveringNode = null;
            } else {
                this.toolbar.show().appendTo(node).position({
                    of: node, at: 'right middle', my: 'right middle', offset: '0 0'
                });
                this.hoveringNode = node;
                this.hoveringNode.addClass('ui-filtereditor-hover-node');
            }
        }
    }

    function nodeMouseLeave(event: Event): void {
        if (this.hoveringNode) this.hoveringNode.removeClass('ui-filtereditor-hover-node');
        this.toolbar.hide();
        this.hoveringNode = null;
    }

    function _createToolbar(): JQuery {
        var result = $('<div>').addClass('ui-filtereditor-toolbar');
        var buttons: [string, string][] = [
            ['move-up', 'ui-icon-carat-1-n'],
            ['move-down', 'ui-icon-carat-1-s'],
            ['condition-add', 'ui-icon-plusthick'],
            ['condition-addgroup', 'ui-icon-folder-open'],
            ['condition-delete', 'ui-icon-minusthick']
        ];
        $.each(buttons, function (i, item) {
            var button = $('<button>').button({icons: {primary: item[1]}});
            button.addClass('ui-filtereditor-toolbutton ui-filtereditor-' + item[0]);
            button.appendTo(result);
        });
        return result;
    }

    function _commitFilter(): void {
        this.root = createElementFromNode(this, this.options.filter);
        this.toolbar.hide().appendTo(this.element);
        this.element.find('.ui-filtereditor-node').remove();
        this.element.append(this.root);
    }

    function _filterChanged(): void {
        this.toolbar.hide();
        this.options.filter = createNodeFromElement(this.root);
        this.element.trigger('filterchange');
    }

    function _toolButtonClicked(event: Event) {
        var node: JQuery = this.hoveringNode;
        var button: JQuery = $(event.currentTarget);
        if (!node || !button.is('button')) return;
        if (button.is('.ui-filtereditor-move-up')) {
            var prev = node.prev('.ui-filtereditor-node');
            if (prev) {
                node.insertBefore(prev);
                _filterChanged.call(this);
            }
        }
        if (button.is('.ui-filtereditor-move-down')) {
            var next = node.next('.ui-filtereditor-node');
            if (next) {
                node.insertAfter(next);
                _filterChanged.call(this);
            }
        }
        if (button.is('.ui-filtereditor-condition-add')) {
            var newElement = createElementFromNode(this, this.options.dummyCondition);
            if (node.is('.ui-filtereditor-group-node')) {
                newElement.appendTo(node);
            } else {
                newElement.insertAfter(node);
            }
            _filterChanged.call(this);
        }
        if (button.is('.ui-filtereditor-condition-addgroup')) {
            var newElement = createElementFromNode(this, {group: 'and', members: [this.options.dummyCondition]});
            if (node.is('.ui-filtereditor-group-node')) {
                newElement.appendTo(node);
            } else {
                newElement.insertAfter(node);
            }
            _filterChanged.call(this);
        }
        if (button.is('.ui-filtereditor-condition-delete')) {
            if (node.is('.ui-filtereditor-comparison-node')) {
                // if this is the only node in a group, delete enclosing group instead
                if (node.siblings('.ui-filtereditor-node').length == 0) {
                    var group = node.parent('.ui-filtereditor-group-node');
                    this._toolbuttonClicked(group, button);
                    return;
                }
            }
            if (node.get()[0] == this.root.get()[0]) {
                // if the target is the top-level group, do nothing
                return;
            }
            this.toolbar.hide().appendTo(this.element);
            node.remove();
            _filterChanged.call(this);
        }
    }

    export function _setOption(key, value) {
        $.Widget.prototype._setOption.apply(this, arguments);
        switch (key) {
            case 'filter':
                _commitFilter.call(this);
                break;
            case 'keys':
                _createKeySelect.call(this);
                _reset.call(this);
                break;
        }
    }

    function createElementFromNode(widget: any, node: Node) {
        var self = widget;

        function createElementFromGroupNode(node: Node): JQuery {
            var elem = $('<div>').addClass('ui-filtereditor-group-node ui-filtereditor-node');
            self.groupSelect.clone().val(node.group).appendTo(elem);
            $.each(node.members, (i, member) => {
                var child = createElementFromNode(self, member);
                child.appendTo(elem);
            });
            elem.sortable({
                axis: 'y',
                start: function () {
                    self.toolbar.hide();
                },
                update: $.proxy(_filterChanged, self),
                items: '> .ui-filtereditor-node'
            });
            return elem;
        }

        function createTypedFieldAndOperatorSelector(selectedKey: string, currentValue: any) {
            var op = $('<select>').addClass('ui-filtereditor-operation-select');
            var keydef = this.keymap[selectedKey];
            $.each(_typeOpMap[keydef.type], (i, opdef) => {
                $('<option>').text(opdef.label).attr('value', opdef.op).appendTo(op);
            });

            var opt = {type: keydef.type, spec: keydef.spec};
            var value = $('<span>').addClass('ui-filtereditor-value').typedfield(opt);
            if (typeof currentValue !== 'undefined') {
                value.typedfield('option', 'value', currentValue);
            }
            return {op: op, value: value};
        }

        function createElementFromComparisonNode(node: Node): JQuery {
            var elem = $('<div>').addClass('ui-filtereditor-comparison-node ui-filtereditor-node');

            var tmpKey = self.keySelect.clone().val([node.key]);
            var op_val = createTypedFieldAndOperatorSelector.call(self, node.key, node.value);
            op_val.value.val(node.condition);
            tmpKey.on('change', (event: Event) => {
                $(event.target).siblings('.ui-filtereditor-operation-select, .ui-filtereditor-value').remove();
                var key = $(event.target).val();
                var op_val = createTypedFieldAndOperatorSelector.call(self, key, undefined);
                elem.append(op_val.op, op_val.value);
            });

            elem.append(tmpKey, op_val.op, op_val.value);
            return elem;
        }

        if (node.members instanceof Array)
            return createElementFromGroupNode(node);
        else if (node.key !== undefined)
            return createElementFromComparisonNode(node);
        else
            throw 'Error';
    }

    function createNodeFromElement(element: JQuery): any {
        function createNodeFromGroupElement(element) {
            var members = [];
            element.children('.ui-filtereditor-node').each(function () {
                var item = createNodeFromElement($(this));
                if (item != null)
                    members.push(item);
            });
            var groupType = $('.ui-filtereditor-group-select', element).val();
            if (members.length > 0)
                return {group: groupType, members: members};
            else
                return null;
        }

        function createNodeFromComparisonElement(element) {
            return {
                key: element.find('.ui-filtereditor-key-select').val(),
                condition: element.find('.ui-filtereditor-operation-select').val(),
                value: element.find('.ui-filtereditor-value').typedfield('option', 'value')
            };
        }

        if (element.is('.ui-filtereditor-group-node'))
            return createNodeFromGroupElement(element);
        else if (element.is('.ui-filtereditor-comparison-node'))
            return createNodeFromComparisonElement(element);
        else
            throw "exception";
    }

    export function exportMongo(element) {
        var self = this;

        function createNodeFromGroupElement(element): any {
            var members = [];
            element.children('.ui-filtereditor-node').each(function () {
                var item = self.exportMongo($(this));
                if (item != null)
                    members.push(item);
            });
            var groupType = $('.ui-filtereditor-group-select', element).val();
            if (members.length > 0) {
                if (groupType == 'and') return {$and: members};
                if (groupType == 'or') return {$or: members};
            } else {
                return null;
            }
        }

        function escapeRegex(str) {
            return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
        }

        function createNodeFromComparisonElement(element) {
            var result = {};
            var key = element.find('.ui-filtereditor-key-select').val();
            var cond = element.find('.ui-filtereditor-operation-select').val();
            var value = element.find('.ui-filtereditor-value').typedfield('option', 'value');
            if (self.keymap[key].type === 'date') {
                var offset = - (new Date()).getTimezoneOffset();
                var sign = offset >= 0 ? '+' : '-';
                offset = Math.abs(offset);
                var hour = ('00' + Math.floor(offset / 60).toString()).slice(-2);
                var min = ('00' + (offset % 60).toString()).slice(-2);
                value = {'$date': value + 'T00:00:00' + sign +  hour + ':' + min };
            }

            switch (cond) {
                case '=':
                    result[key] = value;
                    break;
                case '>':
                    result[key] = {$gt: value};
                    break;
                case '<':
                    result[key] = {$lt: value};
                    break;
                case '>=':
                    result[key] = {$gte: value};
                    break;
                case '<=':
                    result[key] = {$lte: value};
                    break;
                case '!=':
                    result[key] = {$ne: value};
                    break;
                case '*=':
                    result[key] = {$regex: escapeRegex(value)};
                    break;
                case '^=':
                    result[key] = {$regex: '^' + escapeRegex(value)};
                    break;
                case '$=':
                    result[key] = {$regex: escapeRegex(value) + '$'};
                    break;
            }
            return result;
        }

        if (!element) element = self.root;
        if (element.is('.ui-filtereditor-group-node'))
            return createNodeFromGroupElement(element);
        else if (element.is('.ui-filtereditor-comparison-node'))
            return createNodeFromComparisonElement(element);
        else
            throw "exception";
    }
}

$.widget('ui.filtereditor', filterEditor);
