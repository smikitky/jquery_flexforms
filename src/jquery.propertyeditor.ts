/// <reference path="typings/jquery/jquery.d.ts" />
/// <reference path="typings/jqueryui/jqueryui.d.ts" />
/// <reference path="jquery.typedfield.ts" />

module propertyEditorWidget {
    interface Property {
        type: string;
        key: string;
        caption?: string;
        spec?: any;
        heading?: string;
    }

    interface Options {
        properties: Property[];
        value: any;
        useCaptions: boolean;
    }

    export var options: Options = {
        properties: [],
        value: {},
        useCaptions: true
    };

    export function _create() {
        _refreshForm.call(this);
    }

    function _refreshForm() {
        var props = (<Options>this.options).properties;
        this.element.empty().addClass('ui-propertyeditor');
        var table = $('<table>').addClass('ui-propertyeditor-table').appendTo(this.element);

        this.fields = {};
        $.each(props, (i, prop) => {
            if (prop.key in this.fields) return;

            if ('heading' in prop) {
                var row = $('<tr>').addClass('ui-propertyeditor-heading');
                var column = $('<th>').attr('colspan', 2).text(prop.heading).appendTo(row);
                row.appendTo(table);
                return;
            }

            var row: JQuery = $('<tr>').addClass('ui-propertyeditor-row');
            var caption: JQuery = $('<th>').addClass('ui-propertyeditor-caption').text(prop.caption);
            var editor: JQuery = $('<td>')
                .addClass('ui-propertyeditor-column')
                .typedfield({type: prop.type, spec: prop.spec})
                .on('valuechange', (event: Event) => {
                    this.options.value[prop.key] = $(event.target).typedfield('getValue');
                });
            row.append(caption).append(editor).appendTo(table);
            this.fields[prop.key] = editor;
        });
        _assignValues.call(this, this.options.value);
        if (this.options.disabled) {
            disable.call(this);
        }
    }

    export function enable() {
        this.element.find('.ui-propertyeditor-column').typedfield('enable');
        this.options.disabled = false;
    }

    export function disable() {
        this.element.find('.ui-propertyeditor-column').typedfield('disable');
        this.options.disabled = true;
    }

    export function valid() {
        var props = (<Options>this.options).properties;
        var result = true;
        $.each(props, (i, prop) => {
            if ('heading' in prop) return;
            result = result && this.fields[prop.key].typedfield('valid');
        });
        return result;
    }

    export function complain(messages: {[field:string]: string[]}) {
        var key: string;
        this.undoComplain();
        for (key in messages) {
            messages[key].forEach(mes => {
                if (!(key in this.fields)) return;
                var message = $('<p>').addClass('ui-propertyeditor-error').text(mes);
                this.fields[key].closest('td.ui-propertyeditor-column').append(message);
                this.fields[key].closest('ui-propertyeditor-row').addClass('ui-propertyeditor-complained');
            });
        }
    }

    export function undoComplain() {
        $('.ui-propertyeditor-row', this.element).removeClass('ui-propertyeditor.complained');
        $('.ui-propertyeditor-error',this.element).remove();
    }

    export function clear() {
        for (var key in this.fields) {
            this.fields[key].typedfield('reset');
        }
        _refreshValues.call(this);
    }

    export function _setOption(key: string, value: any) {
        switch (key) {
            case 'value':
                _assignValues.call(this, value);
                break;
            case 'disabled':
                if (!!value) {
                    this.disable();
                } else {
                    this.enable();
                }
                break;
            default:
                this._super(key, value);
        }
    }

    function _refreshValues() {
        this.options.value = {};
        var props = (<Options>this.options).properties;
        $.each(props, (i, prop) => {
            if ('heading' in prop) return;
            var field = this.fields[prop.key];
            this.options.value[prop.key] = field.typedfield('valid') ? field.typedfield('getValue') : null;
        });
    }

    function _assignValues(value: any) {
        this.undoComplain();
        for (var key in value) {
            if (typeof this.fields[key] === 'object') {
                this.fields[key].typedfield('setValue', value[key]);
            }
        }
        _refreshValues.call(this);
    }
}

$.widget("ui.propertyeditor", propertyEditorWidget);

interface JQuery {
    propertyeditor: any;
}
