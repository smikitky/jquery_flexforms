/// <reference path="typings/jquery/jquery.d.ts" />
/// <reference path="typings/jqueryui/jqueryui.d.ts" />

module typedField {
    export class Base {
        public changed: () => void;
        protected spec: any;
        protected element: JQuery;

        constructor(spec: Object) {
            this.spec = $.extend({}, spec);
        }

        protected splitKeyLabel(input: string): [string, string] {
            var m: any = null;
            if (m = input.match(/^(.+?)\:(.+)$/)) {
                return [m[1], m[2]];
            } else {
                return [input, input];
            }
        }

        protected assignSpecsToElementProp(keys: string[]) {
            $.each(keys, (i, key) => {
                if (key in this.spec) this.element.prop(key, this.spec[key]);
            });
        }

        protected triggerChanged(): void {
            if (typeof this.changed === 'function') {
                this.changed();
            }
        }

        protected convertScalar(data) {
            if (!('spec' in this) || !('valueType' in this.spec)) {
                return data;
            }
            switch (this.spec.valueType) {
                case 'int':
                    return parseInt(data);
                case 'number':
                    return parseFloat(data);
                case 'boolean':
                    return !!data;
                case 'string':
                    return data.toString();
                default:
                    return data;
            }
        }

        public createElement(): JQuery {
            return null;
        }

        public enable(): void {
        }

        public disable(): void {
        }

        public disabled(): boolean {
            return false;
        }

        public valid(): boolean {
            return true;
        }

        public get(): any {
        }

        public set(value: any): void {
        }

        public reset(): void {
            this.set('');
        }
    }

    export class HtmlInputInput extends Base {
        public get() {
            return this.element.val();
        }

        public set(value: any): void {
            this.element.val(value);
        }

        public enable(): void {
            this.element.prop('disabled', false);
        }

        public disable(): void {
            this.element.prop('disabled', true);
        }

        public disabled(): boolean {
            return this.element.prop('disabled');
        }

    }

    export class TextInput extends HtmlInputInput {
        public createElement(): JQuery {
            this.element = $('<input type="text">').addClass('ui-typedfield-text');
            this.assignSpecsToElementProp(['placeholder', 'length']);
            this.element.on('change input keyup', (event: Event) => {
                this.triggerChanged();
                event.stopPropagation();
            });
            return this.element;
        }

        public valid(): boolean {
            if ('regex' in this.spec) {
                var match = this.element.val().match(this.spec.regex);
                return !!match;
            }
            return true;
        }
    }

    export class PasswordInput extends TextInput {
        public createElement(): JQuery {
            super.createElement();
            this.element.attr('type', 'password');
            return this.element;
        }
    }

    export class NumberInput extends HtmlInputInput {
        static numberSupported: boolean = null;
        private html5: boolean = null;

        private htmlNumberSupported(): boolean {
            if (NumberInput.numberSupported === null) {
                var tmp = $('<input type="number">');
                NumberInput.numberSupported = tmp.prop('type') === 'number'; // supported!
            }
            return NumberInput.numberSupported;
        }

        public valid(): boolean {
            var val = this.element.val();
            if (!val.match(/^-?(0|[1-9]\d*)(\.\d+)?$/)) return false;
            if ('min' in this.spec && this.spec.min > val) return false;
            if ('max' in this.spec && this.spec.max < val) return false;
            if (!this.spec.float && val.match(/\./)) return false;
            return true;
        }

        public get() {
            if (this.spec.float === true) {
                return parseFloat(this.element.val());
            } else {
                return parseInt(this.element.val());
            }
        }

        public set(value: any): void {
            this.element.val(parseInt(<string>value).toString());
        }

        public createElement(): JQuery {
            this.element = $('<input>');
            var useHtml5 = this.htmlNumberSupported && this.spec.html5 !== false;
            this.element.prop('type', useHtml5 ? 'number' : 'text');
            this.assignSpecsToElementProp(['placeholder', 'min', 'max', 'step']);
            this.element.on('change input keyup', () => {
                this.triggerChanged();
                event.stopPropagation();
            });
            return this.element;
        }
    }

    export class Json extends Base {
        public createElement(): JQuery {
            this.element = $('<textarea>').addClass('ui-typedfield-textarea ui-typedfield-json');
            this.element.on('change input keyup', (event: Event) => {
                this.triggerChanged();
                event.stopPropagation();
            });
            return this.element;
        }

        public enable(): void {
            this.element.prop('disabled', false);
        }

        public disable(): void {
            this.element.prop('disabled', true);
        }

        public disabled(): boolean {
            return this.element.prop('disabled');
        }

        public valid(): boolean {
            var val = this.element.val();
            var tmp = null;
            try {
                tmp = JSON.parse(val)
            } catch (e) {
                return false;
            }
            return true;
        }

        public get(): any {
            var val = this.element.val();
            return JSON.parse(val);
        }

        public set(value: any): void {
            this.element.val(JSON.stringify(value, null, '\t'));
        }

        public reset(): void {
            this.set('');
        }
    }

    export class Select extends HtmlInputInput {
        public createElement(): JQuery {
            this.element = $('<select>').addClass('ui-tf-select');
            this.element.on('change', () => {
                this.triggerChanged();
            });
            if ($.isArray(this.spec.options)) {
                var options: Array<string> = this.spec.options;
                for (var i: number = 0; i < options.length; i++) {
                    var tmp = this.splitKeyLabel(options[i]);
                    $('<option>').prop('value', tmp[0]).text(tmp[1]).appendTo(this.element);
                }
            }
            return this.element;
        }

        public get(): any {
            return this.convertScalar(this.element.val());
        }
    }

    export class SelectMultiple extends Select {
        public createElement(): JQuery {
            var element = super.createElement();
            element.prop('multiple', true);
            return element;
        }

        public get(): any {
            var values = this.element.val();
            if (values === null) return [];
            var result = [];
            for (var i: number = 0; i < values.length; i++) {
                result.push(this.convertScalar(values[i]));
            }
            return result;
        }
    }

    export class InputArrayBase extends Base {

        protected inputType: string = 'radio';

        public createElement(): JQuery {
            this.element = $('<div>');
            if (!$.isArray(this.spec.options)) {
                return this.element;
            }
            var options: string[] = this.spec.options;
            for (var i: number = 0; i < options.length; i++) {
                var tmp = this.splitKeyLabel(options[i]);
                var input = $('<input>').prop('type', this.inputType).prop('value', tmp[0]);
                var label = $('<label>').append(input).append(tmp[1]);
                if (this.spec.vertical) label = $('<div>').append(label);
                label.appendTo(this.element);
            }
            return this.element;
        }

        public disable(): void {
            this.element.find(':' + this.inputType).prop('disabled', true);
        }

        public enable(): void {
            this.element.find(':' + this.inputType).prop('disabled', false);
        }

    }


    export class RadioGroup extends InputArrayBase {
        public createElement(): JQuery {
            super.createElement();
            this.element.on('click', '> label > :radio', (event) => {
                this.element.find('> label > :radio').each((i, radio) => {
                    if (event.target !== radio) $(radio).prop('checked', false);
                });
                this.triggerChanged();
            });
            return this.element;
        }

        public valid(): boolean {
            return this.element.find(':radio:checked').length > 0;
        }

        public get(): any {
            return this.convertScalar(this.element.find('> label > :radio:checked').val());
        }

        public set(value: any): void {
            this.element.find('> label > :radio').each((i, radio) => {
                $(radio).prop('checked', $(radio).val() == value)
            });
        }
    }

    export class CheckBoxGroup extends InputArrayBase {
        protected inputType: string = 'checkbox';

        public createElement(): JQuery {
            super.createElement();
            this.element.on('click', ':checkbox', () => {
                this.triggerChanged();
            });
            return this.element;
        }

        public get(): any {
            var result = [];
            this.element.find(':checkbox').each((i, item) => {
                var cb = $(item);
                if ($(cb).is(':checked')) result.push(this.convertScalar(cb.prop('value')));
            });
            return result;
        }

        public set(value: any): void {
            this.element.find(':checkbox').prop('checked', false);
            if (!$.isArray(value)) return;
            $.each(value, (i, item) => {
                this.element.find(':checkbox')
                    .filter((j, cb) => $(cb).prop('value') == item).prop('checked', true);
            });
        }
    }

    export class DatePicker extends Base {
        public createElement(): JQuery {
            this.element = $('<input>').datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: () => this.triggerChanged()
            });
            return this.element;
        }

        public enable(): void {
            this.element.datepicker('enable');
        }

        public disable(): void {
            this.element.datepicker('disable');
        }

        public disabled(): boolean {
            return this.element.datepicker('disabled');
        }

        public get(): any {
            return this.element.val();
        }

        public set(value: any): void {
            this.element.datepicker('setDate', value);
        }

    }

    export class CheckBox extends Base {
        private checkbox: JQuery;

        public createElement(): JQuery {
            this.element = $('<label>');
            this.checkbox = $('<input>').attr('type', 'checkbox').appendTo(this.element);
            if ('label' in this.spec && typeof this.spec.label == 'string') {
                this.element.append(this.spec.label);
            }
            this.checkbox.on('change', () => {
                this.triggerChanged();
            });
            return this.element;
        }

        public enable(): void {
            this.checkbox.prop('disabled', false);
        }

        public disable(): void {
            this.checkbox.prop('disabled', true);
        }

        public disabled(): boolean {
            return this.checkbox.prop('disabled');
        }

        public get(): any {
            return this.checkbox.prop('checked');
        }

        public set(value: any): void {
            this.checkbox.prop('checked', !!value);
        }
    }

    export class ArrayList extends Base {
        private add: JQuery;
        private fields: JQuery;
        private isDisabled: boolean = false;

        public createElement(): JQuery {
            var elem = $('<div>');
            this.add = $('<a>')
                .addClass('ui-icon ui-icon-plusthick')
                .appendTo(elem).
                on('click', () => {
                    this.addElement();
                    this.triggerChanged();
                });
            this.fields = $('<div>').addClass('ui-typedfield-field-list').appendTo(elem);
            this.fields.sortable({
                axis: 'y',
                update: this.triggerChanged.bind(this),
                handle: '.ui-typedfield-list-grip',
                containment: this.fields
            });
            this.element = elem;
            return elem;
        }

        protected addElement(value: any = undefined): void {
            var container = $('<div>').addClass('ui-typedfield-list-container').appendTo(this.fields);
            $('<span>').addClass('ui-icon ui-typedfield-list-grip ui-icon-grip-dotted-vertical').appendTo(container);
            $('<span>')
                .addClass('ui-icon ui-typedfield-list-delete ui-icon-close')
                .appendTo(container)
                .on('click', () => {
                    container.remove();
                    this.triggerChanged();
                });
            var field = $('<span>').typedfield({
                type: this.spec.elementType,
                spec: this.spec.elementSpec
            }).appendTo(container);
            if (typeof value !== 'undefined') {
                field.typedfield('setValue', value);
            }
            field.on('valuechange', (event) => {
                event.stopPropagation();
                this.triggerChanged();
            });
        }

        protected allFields(): JQuery {
            return this.fields.find('.ui-typedfield-list-container > .ui-typedfield');
        }

        public reset(): void {
            this.fields.empty();
        }

        public get(): any {
            if (this.spec.key) {
                var result = {};
                this.allFields().get().forEach(f => {
                    var data = $.extend({}, $(f).typedfield('getValue'));
                    var key = data[this.spec.key];
                    delete data[this.spec.key];
                    result[key] = data;
                });
                return result;
            } else {
                return this.allFields().get().map(f => $(f).typedfield('getValue'));
            }
        }

        public set(value: any): void {
            this.reset();
            if (this.spec.key) {
                if ($.isPlainObject(value)) {
                    $.each(value, (key, val) => {
                        val[this.spec.key] = key;
                        this.addElement(val);
                    });
                }
            } else {
                if ($.isArray(value)) {
                    (<Array<any>>value).forEach((val) => {
                        this.addElement(val);
                    });
                }
            }
        }

        public disable(): void {
            this.allFields().each((i, f) => {
                $(f).typedfield('disable');
            });
            this.isDisabled = true;
        }

        public enable(): void {
            this.allFields().each((i, f) => {
                $(f).typedfield('enable');
            });
            this.isDisabled = false;
        }

        public disabled(): boolean {
            return this.isDisabled;
        }

        public valid(): boolean {
            return !this.allFields().get().some((f) => {
                return !$(f).typedfield('valid');
            });
        }

    }

    export class Form extends Base {
        private isDisabled: boolean = false;

        public createElement(): JQuery {
            this.element = this.spec.form.clone();
            this.element.on('change click keydown input', (event: Event) => {
                event.stopPropagation();
                this.triggerChanged();
            });
            return this.element;
        }

        protected allFields() {
            return this.element.find('[name]');
        }

        public get(): any {
            var data = {};
            $.each(this.allFields(), function() {
                if (this.disabled) return;
                if (/select|textarea/i.test(this.nodeName) || /text|hidden|password/i.test(this.type)) {
                    data[this.name] = $(this).val();
                } else if (this.checked) {
                    if (data[this.name] == undefined) data[this.name] = [];
                    data[this.name].push($(this).val());
                }
            });
            if (typeof this.spec.filter === 'function') data = this.spec.filter(data);
            return data;
        }

        public set(value: any): void {
            $.each(this.allFields(), function() {
                if (value[this.name]) {
                    var val = $(this).val();
                    if(/checkbox|radio/i.test(this.type)) {
                        $(this).prop('checked',
                            $.isArray(value[this.name]) && value[this.name].some(v => v == val));
                    } else {
                        $(this).val(value[this.name]);
                    }
                } else {
                    $(this).val(null);
                }
            });
        }

        public reset() {
            this.set({});
        }

        public enable() {
            $.each(this.allFields(), function() {
                $(this).prop('disabled', false);
            });
            this.isDisabled = false;
        }

        public disable() {
            $.each(this.allFields(), function() {
                $(this).prop('disabled', true);
            });
            this.isDisabled = true;
        }

        public disabled() {
            return this.isDisabled;
        }

        public valid(): boolean {
            if (typeof this.spec.validator === 'function') {
                return this.spec.validator();
            } else {
                return true;
            }
        }
    }

    export class Callback extends Base {
        private data: any = null;
        private content: JQuery;
        private button: JQuery;

        public createElement(): JQuery {
            var element = $('<div>');
            this.content = $('<div>').addClass('ui-typedfield-data');
            element.append(this.content);
            this.button = $('<a>').addClass('ui-typedfield-data-button ui-icon ui-icon-pencil');
            this.button.on('click', () => {
                if (typeof this.spec.edit !== 'function') return;
                this.spec.edit(this.data, (data) => {
                    this.set(data);
                    this.changed();
                });
            });
            element.append(this.button);
            return element;
        }

        public get(): any {
            return this.data;
        }

        public set(value: any): void {
            this.data = value;
            if (typeof this.spec.render === 'function') this.spec.render(this.content, value);
        }

        public reset() {
            this.set(null);
        }

        public enable() {
            this.button.prop('disabled' , false);
        }

        public disable() {
            this.button.prop('disabled', true);
        }

        public disabled() {
            return this.button.prop('disabled');
        }

    }

}

