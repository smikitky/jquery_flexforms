/// <reference path="typings/jquery/jquery.d.ts" />
/// <reference path="typings/jqueryui/jqueryui.d.ts" />
/// <reference path="jquery.typedfield.ts" />

module typedFieldWidget {
    export var options = {
        type: 'text',
        spec: {}
    };

    export function _create() {
        createInput.call(this);
    }

    var map: { [key: string]: typeof typedField.Base } = {
        text: typedField.TextInput,
        password: typedField.PasswordInput,
        number: typedField.NumberInput,
        json: typedField.Json,
        select: typedField.Select,
        selectmultiple: typedField.SelectMultiple,
        checkboxgroup: typedField.CheckBoxGroup,
        date: typedField.DatePicker,
        checkbox: typedField.CheckBox,
        radio: typedField.RadioGroup,
        list: typedField.ArrayList,
        form: typedField.Form,
        callback: typedField.Callback
    };

    export function registerType(type: string, classDefinition: typeof typedField.Base)
    {
        map[type] = classDefinition;
    }

    function createInput(): void {
        this.element.empty();
        this.element.addClass('ui-typedfield ui-typedfield-' + this.options.type);
        if (!(this.options.type in map)) {
            throw 'Undefined field type';
        }

        this.field = new map[this.options.type](this.options.spec);
        var elem = this.field.createElement();
        if ('value' in this.options) {
            this.field.set(this.options.value);
        } else if ('default' in this.options.spec) {
            this.field.set(this.options.spec.default);
        }
        _validate.call(this);
        this.field.changed = () => {
            _validate.call(this);
            this.element.trigger('valuechange', [this.field]);
        };
        elem.appendTo(this.element);
    }

    function _validate() {
        var valid: boolean = this.field.valid();
        this.element.toggleClass('ui-typedfield-invalid', !valid);
        this.options.value = valid ? this.field.get() : null;
    }

    export function enable() {
        this.field.enable();
        this.options.disabled = false;
    }

    export function disable() {
        this.field.disable();
        this.options.disabled = true;
    }

    export function getValue() {
        return this.options.value;
    }

    export function valid() {
        return this.field.valid();
    }

    export function setValue(value: any) {
        this.field.set(value);
        _validate.call(this);
    }

    export function reset() {
        this.field.reset();
        this.field.changed();
    }

    export function _setOption(key: string, value: any) {
        switch (key) {
            case 'type':
                this.options.type = value;
                createInput.call(this);
                break;
            case 'spec':
                this.options.spec = value;
                createInput.call(this);
                break;
            case 'value':
                setValue.call(this, value);
                break;
            case 'disabled':
                if (!!(value)) {
                    this.enable();
                } else {
                    this.disable();
                }
                break;
            default:
                (<any>this)._super(key, value);
        }
    }
}

$.widget("ui.typedfield", typedFieldWidget);

interface JQuery {
    typedfield: any;
}
