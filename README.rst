========================================================
jQuery UI Typed Field & Property Editor &  Filter Editor
========================================================

This is a set of jQuery UI Widgets.

1. **Typed Field** widget creates various types of user input UI,
   corresponding to the option. The other two widgets depend on this.

2. **Filter Editor** enables you to visually build complex
   conditionals such as ``((A > 100 OR B <= 200) AND C != 150) OR D = 50``.
   The result conditional can be exposed as:

   - JSON object (natively understood by this widget)
   - MongoDB-style conditional (JSON object)

3. **Property Editor** creates a "key-value" style form.

------------
Requirements
------------

- jQuery, jQuery UI
- IE >= 8 or any other modern browser

.. important::

   For now, not tested in browsers other than Google Chrome!

----
Demo
----

See the demo in the demo directory.

-----------
Typed Field
-----------

Typed field widget creates various types of user input UI.

Currently, five ``type``s are supported.

- ``text``
- ``number``
- ``select``
- ``checkbox``
- ``date`` (Uses jQuery Datepicker)

-------------
Filter Editor
-------------

How to Use
----------

1. In your HTML file, load jQuery, jQuery UI.

2. Load ``jquery.flexforms.js`` and ``jquery.flexforms.css``.

3. Create ``div`` element, create jQuery object of that element,
   and call ``filtereditor`` method for it::

     var editor = $('#mydiv').filtereditor();

4. The editor triggers ``filterchange`` event whenever there is a meaningful
   change inside the editor. You can access the content of the editor
   as a plain JavaScript object using ``filter`` option::

     editor.on('filterchange', function() {
       var data = editor.filtereditor('option', 'filter');
       $('textarea#result').val(JSON.stringify(data));
     });

5. Additionally, MongoDB-style query condition can be exported, as follows::

     var mongo_where = editor.filtereditor('exportMongo');

   You cannot set back mongo-style objects.

6. You can set the condition, as follows::

     var js_object = {
       group: "and",
       "members": [ { key: "modality", condition: "=", value: "CT" } ]
     };
     editor.filtereditor('option', 'filter', js_object);

Options
-------

``keys``
  The list of available keys and their corresponding value types.
  This is only valid at initialization time, and trying to change this option after initialization will
  cause an error.
  Each key is represented as objects as follows::

    [
      {
        key: 'zipcode',
        label: 'Zip Code',
        type: 'text'
      },
      {
        key: 'age',
        label: 'Your Age',
        type: 'number'
      },
      {
        key: 'sex',
        label: 'Sex',
        type: 'select',
        spec: {
          options: ['Male', 'Female']
        }
      }
    ]

  Current supported types are ``text``, ``number``, ``select`` and ``number``.
  Default: ``[ { value: 'name' }, { value: 'date' } ]``

``filter``
  Readable and writable. The current content of the editor, represented as
  plain JS object.

  Default: "AND" group containing only one dummyCondition.

``dummyCondition``
  Readable and writable. The default node which is inserted when "plus" button
  was clicked.

  Default: ``{ key: keys[0], condition: '=', value: '' }``, where ``keys[0]`` is
  the first key of the key list.


---------------
Property Editor
---------------

How to Use
----------

1. In your HTML file, load jQuery, jQuery UI.

2. Load ``jquery.flexforms.js`` and ``jquery.flexforms.css``.

3. Create ``div`` element, create jQuery object of that element,
   and call ``propertyeditor`` method for it::

     var editor = $('#mydiv').propertyeditor();

4. Handle ``valuechange`` event, which is triggered whenever there is a meaningful
   change inside the property editor. You can access the content of the editor
   as a plain JavaScript object using ``value`` option::

     editor.on('valuechange', function() {
       var data = editor.propertyeditor('option', 'value');
       $('textarea#result').val(JSON.stringify(data));
     });


Options
-------

``properties``
  The list of properties. This can be changed only at initialization process.
  Example::

    var properties = [
      {type: 'text', key: 'name', caption: 'Your Name'},
      {type: 'number', key: 'age', caption: 'Age', spec: { default: 40} },
      {type: 'select', spec: {options: ['Male', 'Female']}, key: 'sex', caption: 'Sex'},
      {type: 'select', spec: {options: ['1', '2', '3'], valueType: 'number'}, key: 'floor', caption: 'Floor'},
      {type: 'date', key: 'birthday', caption: 'Birthday'},
      {type: 'checkbox', key: 'enabled', caption: 'Enabled'}
    ];
    $('#property_editor').propertyeditor({ properties: properties });

  For each elements, ``key`` and ``type`` must be specified. ``caption`` is optional.

``value``
  The current property value.

``disabled``
  If set to true, all the fields will be disabled.

Methods
-------

``disable``
  Disable all the fields.
``enable``
  Enable all the fields.
``clear``
  Clears the contents of all the fields.
``complain(messages)``
  Displays custom error messages, typically sent from validators in the remote server.
  The ``messages`` parameter should look like this::

    { password: ['Too short!', 'Too simple!'], agreement: ['Please check me.'] }
``undoComplain``
  Removes the error messages added by the ``complain`` method.

--------
Building
--------

Original source files are ``*.ts`` (TypeScript), ``*.less`` (LESS), and ``*.jade`` (JADE) files.
JavaScript, CSS and HTML files are compiled from the source files, and are currently included in the
repository.

To build from the source, follow these steps:

1. Install node.js (and node package manager (npm) if it's not installed automatically).

2. Install grunt task runnder globally.::

     npm install -g grunt-cli

3. Install local node packages.::

     cd <path to this repository>
     npm install

4. Run grunt task runner to compile everything.::

     grunt

   Or run one of the grunt tasks specifically.::

     grunt jade
     grunt less
     grunt typescript
     grunt clean
     grunt watch

----
TODO
----

- Validation (Indicates the content of the typed field is invalid)
- More field types (checkbox groups, jQuery UI multiselect, etc...)

-------
License
-------

Modified BSD License.

Copyright 2014 UTRAD-ICAL.